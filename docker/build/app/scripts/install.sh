#!/bin/bash

set -m
set -e

if [ ! -f /var/www/html/artisan ]; then
    echo "Laravel not found! Installing now."
    composer create-project --prefer-dist laravel/laravel .
    composer require market/module-livecoding:0.0.1
fi