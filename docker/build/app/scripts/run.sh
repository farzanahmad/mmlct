#!/bin/bash

set -m
set -e

/docker/scripts/install.sh

composer update

php-fpm --allow-to-run-as-root