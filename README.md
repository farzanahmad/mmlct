Please follow these steps to setup the instance.

1. Clone the repo (`git clone git@gitlab.com:farzanahmad/mmlct.git market`)
2. Go inside repository directory
3. `cd docker`
4. `docker-compose build`
5. `docker-compose up` or `docker-compose build -d`, Upon first initiating, it will install laravel and a custom module (so it will take sometime, you can have a ☕️ meanwhile)
6. all set
7. login to your container `docker exec -it mmlct_app bash`
8. try the customer filter command `php artisan market:filter id --arg1=5 --arg2=10 --source=http://www.mocky.io/v2/5c6abed9330000cc2e7f4ceb`
